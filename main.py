#!/usr/bin/env python3

import sys
from subprocess import call
from tempfile import NamedTemporaryFile as NTF

URL = "http://0.0.0.0:9981/stream/channelid/{}"
PLAYER = "mpv"
STREAMS = {
    "tvp1": 1523591165,
    "tvp2": 586726732,
    "tvp3": 267332189,
    "tv4": 124512375,
    "tvn": 50424256,
    "tv6": 45476305,
    "tvn7": 280039017,
    "polsat": 1961184178,
    "spolsat": 509384827,
    "ttv": 46790086,
    "fokus": 1848621092,
    "sklatka": 1991693003,
    "kultura": 347299772,
    "historia": 302963042,
    "rozrywka": 1960606302,
    "metro": 204162283,
    "atm": 1753452813,
    "zoom": 1058359208,
    "wp": 85556400,
    "nowa": 1482146093,
    "puls": 445584874,
    "puls2": 2055402682,
    "info": 768448716,
    "abc": 1646016245
}

def make_temp_playlist():
    with NTF("w", delete=False) as playlist_file:
        playlist_file.write('#EXTM3U\n')

        for key, val in STREAMS.items():
            playlist_file.write('#EXTINF:-1,{}\n{}\n'.format(key, URL.format(val)))

        return playlist_file.name

def play(stream=None):
    if not stream:
        call([PLAYER, make_temp_playlist()])
    else:
        if stream.lower() in STREAMS:
            call([PLAYER, URL.format(STREAMS[stream.lower()])])

if __name__ == '__main__':
    if len(sys.argv) > 1:
        print("Playing ", sys.argv[1])
        play(sys.argv[1])
    else:
        play()
